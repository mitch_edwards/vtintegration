## VirusTotal Notification Sync
This jobs app is designed to run daily to keep your hunting notifications and respective files from VirusTotal updated in ThreatConnect.

### DataStore Preperation
The app requires a mapping json file that it will pull from the datastore under the `virustotal` data type and the rid `notification_mappings`. Please use the playbook found in this repo to help you update the mapping in the datastore.

### Building and Deploying
Make sure the newest `tcex` is installed. Navigate into the `TC_-_Notification_Sync` directory and run `tclib` and then `tcpackage`. A file `TC_-_Rule_Notification.tcx` will be created under the `target` directory. Install this file on your ThreatConnect instance in TC Exchange with a system admin account.

### Job App Configuration
Pretty self-explanatory, just the VT API key and target owner (where notifications should be populated).

#### Notes
* This app requires that the rule sync app has been run before.