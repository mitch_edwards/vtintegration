# -*- coding: utf-8 -*-
"""Job Args"""


class Args(object):
    """Job Args"""

    def __init__(self, parser):
        """Initialize class properties."""
        parser.add_argument('--owner', required=True)
        parser.add_argument('--vt_api_key', required=True)
