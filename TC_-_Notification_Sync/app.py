# -*- coding: utf-8 -*-
"""ThreatConnect Job App"""

import requests
import json

from tc_dm import map_and_create
from job_app import JobApp

DATASTORE_HOME = 'virustotal'
DATASTORE_MAPPINGS = 'notification_mappings'
DATASTORE_LAST_ID = 'last_id'
DATASTORE_ID_LOOKUP_TABLE = 'id_lookup_table'

VT_NOTIFICATIONS_URL = 'https://www.virustotal.com/api/v3/intelligence/hunting_notification_files'

MAPPINGS = {
"attributes": {"description" : "Description", "priority": "Rule Priority", "confidence" : "Rule Confidence"},
"tags": True,
"boolean_tags": ["sandbox_restricted"],
"associations": {"threats" : "Threat"},
"default_association": "Threat"
}


headers_post = {'Content-Type': 'application/json', 'DB-Method': 'POST'}
headers_get = {'Content-Type': 'application/json', 'DB-Method': 'GET'}
def get_notifications(vt_api_key, limit=None, cursor=None, vt_filter=None, daysbefore = 30):
    params = {}
    target_date = datetime.datetime.now() - datetime.timedelta(days=daysbefore)
    if limit:
        params['limit'] = limit
        params['start_date'] = target_date
    if vt_filter:
        params['filter'] = vt_filter
    if cursor:
        params['cursor'] = cursor
    response = requests.get(
        VT_NOTIFICATIONS_URL,
        headers={'x-apikey': vt_api_key},
        params=params
    ).json()
    new_cursor = response.get('meta', {'cursor': None}).get('cursor')
    yield response['data']
    if new_cursor:
        yield from get_notifications(vt_api_key, limit, new_cursor, vt_filter)
    else:
        yield None


class App(JobApp):
    """Job App"""

    def createifnotexist(self, domain='organization',data_type='vt_mappings'):
        headers_get = {'Content-Type':'application/json', 'DB-Method':'GET'}

        #checkexist = self.tcex.session.post('/v2/exchange/db/{}/{}/_search'.format('organization', 'vt_mappings'), json = {}, headers=headers_get)
        checkexist = self.tcex.datastore(domain, 'vt_mappings').get(rid='vt_mappings')
        #self.tcex.log.info('[-] createifnotexist() checkexist: '+str(checkexist))
        if not checkexist['found']:
            self.tcex.log.info('[-] Datastore not found...')

            index_settings = {}
            #headers = {'Content-Type': 'application/json', 'DB-Method': 'POST'}
            #url = '/v2/exchange/db/{}/{}/{}'.format('organization', 'vt_mappings', 'vt_mappings')
            #res = self.tcex.session.post(url, json=index_settings, headers=headers)
            res = self.tcex.datastore(domain, 'vt_mappings').add(rid='vt_mappings', data=MAPPINGS)
            if res.ok:
                self.tcex.log.info('[-] Datastore creation request sent, checking for datastore...')
                checkexist = self.tcex.datastore(domain, 'vt_mappings').get(rid='vt_mappings')
                if checkexist.ok:
                    self.tcex.log.info('[-] Datastore successfully created!')
                    return True
                else:
                    self.tcex.log.error('[x] Datastore error!')
                    self.tcex.log.error('[x] '+checkexist.text)
                    return False
            else:
                self.tcex.log.info('[x] Datastore not created successfully...')
                return False
        else:
            self.tcex.log.info('[-] Datastore found!')
            return True

    def pushifempty(self, domain='organization', data_type='vt_mappings'):

        #ds_get = self.tcex.session.post('/v2/exchange/db/{}/{}/{}/_search'.format(domain, data_type, 'vt_mappings'), json = {}, headers=headers_get)
        ds_get = self.tcex.datastore(domain, 'vt_mappings').get(rid='vt_mappings')
        if ds_get['found']:
            try:
                
                attributes = ds_get['_source']['attributes']
                self.tcex.log.info('[-] Successfully accessed data in data store!')
                return True
            except Exception as e:
                self.tcex.log.error('[x] Datastore appears to be empty!')
                self.tcex.log.error('[x] Error: {}'.format(str(e)))
                self.tcex.log.error('[x] DS: '+str(ds_get))
                url = '/v2/exchange/db/{}/{}/{}/'.format(domain,data_type, 'vt_mappings')
                put_result = self.tcex.session.post(url, json=MAPPINGS, headers=headers_post)
                if put_result.ok:
                    try:
                        response_data = put_result.json()
                        self.tcex.log.info('[-] Successfully pushed to datastore! Printing datastore object...')
                        self.tcex.log.info('[-] '+str(response_data))

                        checkexist = self.tcex.session.post('/v2/exchange/db/{}/{}/{}/'.format(domain, data_type, 'vt_mappings'), json = {}, headers=headers_get)
                        if checkexist.ok:
                            try:
                                json_obj = checkexist.json()['_source']
                                attributes = json_obj['attributes']
                                self.tcex.log.info('[-] Successfully accessed json data, datastore present and loaded!')
                                return True
                            except Exception as E:
                                self.tcex.log.error('[x] Error attempting to access data in datastore, printing datastore object...')
                                self.tcex.log.error('[x] '+str(json_obj))
                                self.tcex.log.error('[x] Error: '+str(E))
                                self.tcex.log.error('[x] JSon Blob: '+str(json_obj))
                                return False
                    except Exception as E:
                        self.tcex.log.error('[x] Error: '+str(E))
                        self.tcex.log.error('[x] response_data: '+str(response_data))
                else:
                    self.tcex.log.error('[x] Could not push to datastore!')
                    self.tcex.log.error('[x] '+checkexist.text)
                    return False
        else:
            self.tcex.log.error('[x] Datastore not found for some reason')
            self.tcex.log.error('[x] '+ds_get.text)
            return False
    def run(self):
        self.tcex.log.info('[-] Notification sync starting')
        self.tcex.log.info('[-] Checking for datastore...')
        
        # This checks to see if the vt_mappings object exists in the datastore (and creates it
        # if it isn't present) and checks if it is empty (and populates it if so) 
        if self.createifnotexist('organization', 'vt_mappings') and self.pushifempty('organization', 'vt_mappings'):
            self.tcex.log.info('[-] Datastore successfully pushed and populated!')
        else:
            self.tcex.log.error('[x] Datastore push or population had errors!')
        
        #This line returns the datastore object to be used in the rest of the program
        ds = self.tcex.datastore('organization', 'vt_mappings')

        #Check for last_id, push if it doesn't exist
        
        last_id = {}
        try:
            last_id = ds.get(rid=DATASTORE_LAST_ID)['_source']
            self.tcex.log.info('[-] Fetched last_id from datastore! : '+str(last_id))
            last_id = last_id['id']
        except:
            last_id = {'id':'None'}
            ds.add(rid=DATASTORE_LAST_ID, data=last_id)
            last_id = ds.get(rid=DATASTORE_LAST_ID)['_source']
            self.tcex.log.error('[x] last_id: '+str(last_id))
            last_id = last_id['id'] #This is no longer the problem area

        #This section returns the data structures returned by the get_notifications() function, which 
        #returns an iterable data structure full of VT Notification data structures with a default limit 
        #of 40 Notifications per iteration. 
        notifications = get_notifications(self.args.vt_api_key, limit=40)
        data = next(notifications)
        first_id = data[0]['context_attributes']['notification_id']
        #This section iterates over the entirety of the notifications and trims it to the last_id value 
        count = 0
        last_found = False
        while data:
            if last_id:
                ids = [i['context_attributes']['notification_id'] for i in data]
                
            
            for point in data:
                
                tcobj = {}

                """
                MAPPINGS = {
                    "attributes": {
                        "description" : "Description", 
                        "priority": "Rule Priority", 
                        "confidence" : "Rule Confidence"},
                    "tags": True,
                    "boolean_tags": ["sandbox_restricted"],
                    "associations": {"threats" : "Threat"},
                    "default_association": "Threat"
                }
                """

                """
                This is the bit that will need to be changed when/if the TC mappings or VirusTotal 
                data structure changes. It isn't a perfect solution as it will require minor changes 
                to the code itself in the infrequent case of the data structures changing, but what 
                it sacrifices in efficiency, it makes up for in transparency and understandability.

                The process to change the code involves mapping the key (k) to its associated item in
                the VT data structure. It is relatively trivial to find the associated data structure,
                but for further ease, the first log after this comment will print out the VT data point 
                in its JSON form. 
                """
                self.tcex.log.info('[-] Data Point: '+str(point))
                for k, v in MAPPINGS['attributes'].items():
                    self.tcex.log.info('[-] k : v - '+str(k)+' : '+str(v))
                    if k == 'description':
                        try:
                            desc = point['attributes']['crowdsourced_yara_results'][0][k]
                            self.tcex.log.info('[-] Found description! '+str(point))
                        except:
                            desc = 'No description provided...'
                            
                        tcobj[v] = desc
                    if k == 'priority':
                        try:
                            priority = point['attributes'][k]
                            self.tcex.log.info('[-] Found priority! '+str(point))
                        except:
                            priority = 'No priority provided...'
                        Rule_Priority = priority
                        tcobj[v] = Rule_Priority
                    if k == 'confidence':
                        try:
                            Rule_Confidence = point['attributes'][k]
                            self.tcex.log.info('[-] Found rule confidence! '+str(point))
                        except:
                            Rule_Confidence = 'No rule confidence found...'
                        tcobj[v] = Rule_Confidence 
                    
                tcobj['tags'] = True 
                tcobj['boolean_tags'] = ['sandbox_restricted']
                try:
                    threat = point['threats']
                except:
                    threat = 'Threat not found...'

                tcobj['associations'] = {}
                tcobj['associations']['Threat'] = threat   
                tcobj['default_associations'] = 'Threat'
                try:
                    data = next(notifications)
                    self.tcex.log.info('[-] TC Object # '+str(count)+': '+str(tcobj))
                    count = count+1
                except:
                    break
                
            ds.add(rid=DATASTORE_LAST_ID, data={"id": first_id})

            
        ds.add(rid=DATASTORE_LAST_ID, data={"id": first_id})
        self.tcex.log.info('[-] Pushed last_id to datastore! : '+str({'id':first_id}))
        #TODO: Push relevant TC data. 

        
"""        

class App_Cez(JobApp):
    Job App
    
    def run(self):
        self.tcex.log.info('Notification Sync Starting.')
        ds = self.tcex.datastore('organization', DATASTORE_HOME)
        ### This part needs to be changed to the custom_metric method
        last_id = self.tcex.session
        last_id = {}
        try:
            last_id = ds.get(rid=DATASTORE_LAST_ID)['_source']
        except RuntimeError as e:
            ds.add(rid=DATASTORE_LAST_ID, data=last_id)
        last_id = last_id.get('id')

        ###This part needs to be changed to the hard-coded JSON mappings method
        try:
            ilt = ds.get(rid=DATASTORE_ID_LOOKUP_TABLE)['_source']
        except RuntimeError as e:
            self.tcex.exit(1, 'Error fetching ID Lookup: {} Did you forget to run the rule sync?'.format(e.args[1]))
        try:
            mappings = ds.get(rid=DATASTORE_MAPPINGS)['_source']
            if isinstance(mappings, str):
                mappings = json.loads(mappings)
            if 'data' in mappings:
                mappings = mappings['data'] if isinstance(mappings['data'], dict) else json.loads(mappings['data'])  # probably not necessary but just to be safe
        except RuntimeError as e:
            self.tcex.exit(1, 'Problem fetching mapping: {}'.format(e.args[1]))  # tcex log


        count = 0
        last_found = False
        notifications = get_notifications(self.args.vt_api_key, limit=40)
        data = next(notifications)
        first_id = data[0]['context_attributes']['notification_id']
        while data:
            #WE ARE HERE
            print('processing batch {}'.format(count))
            if last_id:
                ids = [i['context_attributes']['notification_id'] for i in data]
                try:
                    ind = ids.index(last_id)
                except ValueError:
                    pass
                else:
                    last_found = True
                    data = data[:ind]
            map_and_create(mappings, data, self.tcex, self.args.owner, count)
            for d in data:
                a = d['attributes']
                c = d['context_attributes']
                r_set = c['ruleset_name']
                r_name = c['rule_name']
                sig_id = ilt[r_set][r_name]['latest']
                sig_groups = ilt[r_set][r_name]['groups']

                file_ti = self.tcex.ti.indicator(indicator_type='File', owner=self.args.owner, unique_id=a['sha256'])
                sig_ti = self.tcex.ti.group(group_type='Signature', owner=self.args.owner, unique_id=sig_id)
                sig_ti.add_association(target=file_ti)
                for i in filter(lambda x: x['type'] == 'Incident', file_ti.group_associations()):
                    inc_ti = self.tcex.ti.group(group_type='Incident', owner=self.args.owner, unique_id=i['id'])
                    inc_ti.add_association(target=sig_ti)
                for sg in sig_groups:

                    if len(self.tcex.ti.group(group_type=sg['type'], owner = self.args.owner, unique_id=sg['id'])) == 0:
                        group_ti = self.tcex.ti.group(group_type=sg['type'], owner=self.args.owner, unique_id=sg['id'])
                    else:
                        group_ti = self.tcex.ti.group(group_)
                    group_ti.add_association(target=file_ti)
            if last_found:
                break
            count += len(data)
            data = next(notifications)
        ds.add(rid=DATASTORE_LAST_ID, data={"id": first_id})
        self.tcex.log.info('Notification Sync Complete.')
"""